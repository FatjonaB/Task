import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Users from '@/views/User/Views/UserList.vue'
Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/users',
    name: 'Users',
    component: Users,
  },
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes,
})

export default router
